import 'package:flutter/cupertino.dart';

class ConstantsImages {
  static const Color colorAzulUniajc = Color(0xFF006cd2);
  static const Color colorNegroUniajc = Color(0xFF212121);
  static const Color colorGrisPalidoUniajc = Color(0xFFf6f6f8);
  static const Color colorBlancoUniajc = Color(0xFFffffff);
  static const Color colorAmarilloUniajc = Color(0xFFf8e100);
  static const Color colorAzulClaroDosUniajc = Color(0xFF0065c7);
  static const Color colorGrisMedioUniajc = Color(0xFF848483);
  static const Color colorAzulbebeUniajc = Color(0xFFb7ceff);
  static const Color colorArandanoUniajc = Color(0xFF444c94);
  static const Color colorGrisAzulOscuroUniajc = Color(0xFF253551);
  static const Color colorAzulClaroUniajc = Color(0xFF0168c5);
  static const Color colorBrownishUniajc = Color(0xFFa77860);
  static const Color colorGridCalidoUniajc = Color(0xFF9e9e9e);
  static const Color colorAzulDodgerUniajc = Color(0xFF518afa);
  static const Color colorBlushUniajc = Color(0xFFeea886);
  static const Color colorBronceUniajc = Color(0xFF4d4f5c);
  static const Color colorAzulGrisOscuroUniajc = Color(0xFF303669);
  static const Color colorGrisCalidoDosUniajc = Color(0xFF848383);
  static const Color colorBlaco67Uniajc = Color(0xFFf8f8f8);
  static const Color colorGrisCarbonUniajc = Color(0xFF3a3f47);
  static const Color colorRojoPastelUniajc = Color(0xFFe55353);

  static const String urlBaseEmojic = "lib/src/resources/images/emojins/1x/";
  static const String emojic1 = urlBaseEmojic +
      "1-2.5mdpi (Conflicted copy from MicroinformaticaÔÇÖs iMac on 2020-09-05).png";
  static const String emojic2 = urlBaseEmojic +
      "2,5-3mdpi (Conflicted copy from MicroinformaticaÔÇÖs iMac on 2020-09-05).png";
  static const String emojic3 = urlBaseEmojic +
      "3-3,5mdpi (Conflicted copy from MicroinformaticaÔÇÖs iMac on 2020-09-05).png";
  static const String emojic4 = urlBaseEmojic +
      "45-5mdpi (Conflicted copy from MicroinformaticaÔÇÖs iMac on 2020-09-05).png";
  static const String emojic5 = urlBaseEmojic +
      "indefinidomdpi (Conflicted copy from MicroinformaticaÔÇÖs iMac on 2020-09-05).png";
}
