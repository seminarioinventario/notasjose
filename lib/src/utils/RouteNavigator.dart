import 'package:flutter/material.dart';
import '../ui/login/login_ui.dart';
import '../ui/login/loginPass_ui.dart';

class RouteNavigator {
  static void goToRoot(BuildContext context) {
    Navigator.pushNamed(context, "/");
  }

  static void goToLoginUser(BuildContext context) {
    Navigator.of(context)
        .pushNamedAndRemoveUntil("/login_ui", (Route<dynamic> route) => false);
  }

  /*static void gotoPasswordUi(BuildContext context) {
    Navigator.of(context).pushNamedAndRemoveUntil(
        "/loginPass_ui", (Route<dynamic> route) => false);
  }*/

  static void goToHome(BuildContext context) {
    Navigator.of(context)
        .pushNamedAndRemoveUntil("/home_ui", (Route<dynamic> route) => false);
  }

  static void goToeditPassword(BuildContext context) {
    Navigator.of(context)
        .pushNamedAndRemoveUntil("", (Route<dynamic> route) => false);
  }
}
