class ConstantsUrl {
  static const String serverUrl1 = "smartcampus.uniajc.edu.co:8888";
  static const String baseLoginUrl =
      "/RSU_LoginClient-0.0.2-SNAPSHOT/login/api/";
  // static const String serverUrl = "smartdev.uniajc.edu.co:8888";
  //static const String serverUrl2 = "smartcampus.uniajc.edu.co:8888";
  static const String serverUrl2 = "smartdev.uniajc.edu.co:8888";
  //static const String baseLoginUrl =
  //    "/RSU_LoginClient-0.0.2-SNAPSHOT/login/api/";
  //static const String consultarNota = "http://smartcampus.uniajc.edu.co:8888/rse_notasuniajc-1.0-SNAPSHOT/swagger-ui.html#/";
  static const String consultarNota = "/rse_notasuniajc-1.0-SNAPSHOT/";

  static const String validateUserUrl = baseLoginUrl + "validateUser";
  static const String authenticateTokenUrl =
      baseLoginUrl + "authenticationToken";
  static const String authenticateCredentialUrl =
      baseLoginUrl + "authentication";
  static const String olvideClaveUrl = baseLoginUrl + "olvideClave";
  static const String refreshTokenUrl = baseLoginUrl + "refreshTokenUser";
  static const String historicoNotaUrl = consultarNota + "historicoNotas/";
  static const String notasActualUrl = consultarNota + "notasactual/";
  //static const String notasActualUrl = consultarNota + "notasactual/";
  static const String programaAcademicoUrl =
      consultarNota + "programaacademico/";
  static const String sistemCalifAlternUrl =
      consultarNota + "sistemcalifaltern/";
  static const String sistemadeCalificacionUrl =
      consultarNota + "sistemadecalificacion/";
}
