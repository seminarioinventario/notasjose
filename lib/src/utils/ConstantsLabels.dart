class ConstantsLabels {
  static const String showPasswdLabel = 'Mostrar password';
  static const String hidePasswdLabel = 'Ocultar password';
  static const String informationHelp = "Información";
  static const String acceptLabel = "Aceptar";
  static const String continueLabel = "Continuar";
  static const String cancelLabel = "Cancelar";
  static const String buscarLabel = "Buscar";
  static const String atencionLabel = "Atención";
  static const String campoRequeridoLabel = "Campo Requerido";

  static const String siLabel = "Si";
  static const String noLabe = "No";
  static const double sizeLabelButton = 14;

  static const double sizeLabelOption = 17;
  static const String keyPegeId = "pegeId";
  static const String keyToken = "token";
  static const String keyList = "list";
  static const String keyName = "nombre";
  //static const int keyCodigo=0;
  //Key
  /*static const String keyPegeId = "pegeId";
  static const String keyIdentificacion = "Identificacion";
  static const String keyId = "id";
  static const String keyEstadoPlan = "estadoPlan";
  static const String keyToken = "token";
  static const String keyName = "nombre";
  static const String keyUserName = "username";
  static const String keyListObjects="listServices";
  static const String keyList="list";
  static const String keyListHist="listHist";
*/

  //Titles
  static const String titleAcercade = "Acerca de";
  static const String titlePreguntasF = "Preguntas Frecuentes";
  /*static const String title = "My app";
  static const String titleServicios = "Servicios";
  static const String titleOpciones = "Opciones";
  static const String titleCrear = "Crear";
*/

  //Menú Opciones
/*  static const String titleHistorial = "Historial";
  static const String titleCerrarSesion = "Cerrar Sesión";
  static const String msgCerrarSesion = "¿Desea cerrar sesión?";
*/
  static const String msgCerrarSesion = "¿Desea cerrar sesión?";

  //Exceptions msg
  static const int codeErrorInternet = 600;
  static const String msgNotConnectioInternet = "No hay Conexión a internet";

  static const int codeErrorApi = 700;
  static const String msgNotConnectioApi =
      "Lo sentimos, servidor en mantenimiento";

  static const int codeError = 800;
  static const String msgError =
      "Lo sentimos, se ha presentado un error, por favor intenta más tarde";

  static const int codeErrorJWTEpired = 501;
  static const String msgerrorJWTEpired = "Su sesión ha expirado";

  static const Pattern pattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
/*
  static const String acercade =
      "Acerca de la app";
  static const String desarrollado = "Desarrollado por:";
  */
}
