import 'dart:convert';
import 'package:expansion_card/expansion_card.dart';
import 'package:flutter_rounded_progress_bar/flutter_rounded_progress_bar.dart';
import 'package:flutter_rounded_progress_bar/rounded_progress_bar_style.dart';
import 'package:notas_uniajc/src/Bloc/ConsultarNotasBloc.dart';
import 'package:notas_uniajc/src/models/ObjApiResponse.dart';

import 'package:http/src/response.dart';
import 'package:notas_uniajc/src/models/SessionNotas/NotasActualesModel.dart';
import 'package:notas_uniajc/src/models/SessionNotas/ProgramaAcaModel.dart';
import '../models/SessionNotas/NotasActualModel.dart';

import 'ConstantsImages.dart';
import 'ConstantsLabels.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'RouteNavigator.dart';
import 'package:notas_uniajc/src/ui/home/homePage/nota_ui.dart';
//import 'package:notas_uniajc/src/ui/login/loginUsuUi.dart';

class CommonsWidget {
  String formatDateWhitTime(String date) {
    DateTime initialTimeSession = DateTime.parse(date);

    return DateFormat("yyyy-MMM-dd  hh:mm a").format(initialTimeSession);
  }

  ObjApiResponse objResponse(res) {
    ObjApiResponse _objApiResponse = ObjApiResponse();
    var resBody = jsonDecode(utf8.decode(res.bodyBites));
    switch (res.statusCode) {
      case 500:
        _objApiResponse.codigo = 500;
        _objApiResponse.mensaje = "error 500";
        if (resBody["message"].toString().contains("JWT")) {
          _objApiResponse.codigo = 501;
          _objApiResponse.mensaje = "Error del jwt session";
        }
        break;
      default:
        _objApiResponse = ObjApiResponse.fromJson(resBody);
    }
    return _objApiResponse;
  }

  showAlertDialogButton(context, tittle, message) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(tittle),
          content: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Text(ConstantsLabels.acceptLabel),
              onPressed: () {
                Navigator.of(context).pop();
                RouteNavigator.goToLoginUser(context);
              },
            ),
          ],
        );
      },
    );
  }

  showAlertDialogOneButton(context, tittle, message) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(tittle),
          content: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Text(ConstantsLabels.acceptLabel),
              onPressed: () {
                Navigator.of(context).pop();
                RouteNavigator.goToLoginUser(context);
              },
            ),
          ],
        );
      },
    );
  }

  showAlertDialogOneButton1(context, title, message, closeScreen) {
    // set up the button
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          shape: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
          ),
          title: Center(
              child: Column(
            children: <Widget>[
              Icon(
                Icons.error_outline,
                color: Color.fromRGBO(240, 173, 78, 1),
                size: 60,
              ),
              const SizedBox(height: 10.0),
              Text(
                title,
                style: GoogleFonts.raleway(
                    fontWeight: FontWeight.bold, fontSize: 20),
              ),
            ],
          )),
          content: Container(
            width: double.maxFinite,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Center(
                      child: Text(
                    message,
                    textAlign: TextAlign.center,
                  )),
                  const SizedBox(height: 30.0),
                  Center(
                    child: SizedBox(
                      width: 100.0,

                      ///BTN Save
                      child: RawMaterialButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20.0)),
                        ),
                        // fillColor: ConstantsImages.colorAzulUniajc,
                        splashColor: Colors.blueAccent,
                        child: Text(
                          ConstantsLabels.acceptLabel,
                          style: Theme.of(context).textTheme.button.copyWith(
                                color: Colors.white,
                              ),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();

                          //Cerrar la pantalla actual
                          if (closeScreen) {
                            Navigator.pop(context, true);
                          }
                        },
                      ),
                    ),
                  ),
                ]),
          ),
        );
      },
    );
  }

  Widget cargaLista(
      List<ProgramaModel> snapData1,
      ConsultarNotasBloc consultarNotasBloc,
      TextEditingController editingController,
      BuildContext context,
      TextTheme textTheme) {
    return Column(
      children: <Widget>[
        (snapData1.isEmpty)
            ? Expanded(
                child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[Text("Notas")],
                ),
              ))
            : Expanded(
                child: ListView.separated(
                  separatorBuilder: (BuildContext context, int index) =>
                      Divider(),
                  itemCount: snapData1.length,
                  padding: EdgeInsets.fromLTRB(8.0, 20.0, 8.0, 8.0),
                  itemBuilder: (BuildContext context, int index) {
                    return ExpansionCard(
                      borderRadius: 20,
                      background: Image.asset(
                        "",
                        fit: BoxFit.cover,
                      ),
                      title: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              snapData1[index].nombrePrograma,
                              style: TextStyle(
                                fontFamily: 'Raleway-SemiBold',
                                color: ConstantsImages.colorBlancoUniajc,
                                backgroundColor:
                                    ConstantsImages.colorAzulClaroUniajc,
                              ),
                            ),
                            RoundedProgressBar(
                              style: RoundedProgressBarStyle(
                                  borderWidth: 0, widthShadow: 0),
                              margin: EdgeInsets.symmetric(vertical: 16),
                              borderRadius: BorderRadius.circular(24),
                              percent: 0,
                            ),
                            Text(
                              snapData1[index].materias.first.descripMateria,
                            ),
                          ],
                        ),
                      ),
                      children: <Widget>[
                        Container(
                          child: Text(
                            "linea 221",
                            //snapData1[index].materias[index].descripMateria,
                          ),
                        ),
                        Container(
                          child: Text(
                            "linea 226",
                            /*snapData1[index]
                                .materias[index]
                                .calificaciones[index]
                                .evacDescripcion,*/
                          ),
                        ),
                        Container(
                          child: Text(
                            snapData1[index].nombrePrograma,
                            /* snapData1[index]
                                .materias[index]
                                .calificaciones[index]
                                .nota
                                .toString(),*/
                          ),
                        )
                      ],
                    );
                  },
                ),
              ),
      ],
    );
  }

  Widget cargaListaPrograma(
      List<ProgramaAcademicoModel> snapData,
      ConsultarNotasBloc consultarNotasBloc,
      TextEditingController editingController,
      BuildContext context,
      TextTheme textTheme) {
    return Column(
      children: <Widget>[
        (snapData.isEmpty)
            ? Expanded(
                child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[Text("Historico")],
                ),
              ))
            : Expanded(
                child: ListView.separated(
                  separatorBuilder: (BuildContext context, int index) =>
                      Divider(),
                  itemCount: snapData.length,
                  padding: EdgeInsets.fromLTRB(8.0, 20.0, 8.0, 8.0),
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                      title: Text(snapData[index].nombre),
                      subtitle: Text(snapData[index].codigo),
                      onTap: () {
                        /* MaterialPageRoute(
                            builder: (context) => MaterisdHistoricoUi(
                                  ProgramaAcademicoModel: snapData[index].codigo,
                                ));*/
                      },
                    );
                  },
                ),
              ),
      ],
    );
  }
}
