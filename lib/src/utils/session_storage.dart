import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class SessionStorage {
  static final _storage = FlutterSecureStorage();

  Future<Map<String, String>> getAllValues() async {
    return _storage.readAll();
  }

  Future<String> getValueforKey(String key) async {
    return _storage.read(key: key);
  }

  Future deleteValue(String key) async {
    await _storage.delete(key: key);
  }

  Future deleteAllValues() async {
    await _storage.deleteAll();
  }

  Future writeValue(String key, String value) async {
    await _storage.write(key: key, value: value);
  }

  Future saveIdUser(int pegeId) async {
    await _storage.write(key: 'pegeId', value: pegeId.toString());
  }

  Future<String> getUsername() async {
    return _storage.read(key: 'nombre');
  }

  Future<int> getIdUser() async {
    return int.parse(await _storage.read(key: 'pegeId'));
  }

  Future<String> getAccessToken() async {
    return _storage.read(key: 'accessToken');
  }

  Future<int> getexpiryDuration() async {
    String exp = await _storage.read(key: 'expiryDuration');
    return int.parse(exp);
  }
}
