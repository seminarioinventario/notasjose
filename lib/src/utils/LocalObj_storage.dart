import 'package:localstorage/localstorage.dart';

class LocalObjStorage {
 

  final LocalStorage _storage = LocalStorage('myapp');


  Future<dynamic> getValueforKey(String key) async {
    return _storage.getItem(key);
  }

  Future deleteValue(String key) async {
    await _storage.deleteItem(key);
  }

  Future deleteAllValues() async {
    await _storage.clear();
  }

  Future writeValue(String key, value) async {
     await _storage.setItem(key, value);
  }


}
