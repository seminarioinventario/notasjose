import 'package:notas_uniajc/src/apiservices/LoginApiService.dart';
import 'package:notas_uniajc/src/repository/ManageSessionData.dart';
import 'package:notas_uniajc/src/models/ObjApiResponse.dart';
import 'package:notas_uniajc/src/models/session/SessionCredential.dart';
import 'package:notas_uniajc/src/models/session/SessionUser.dart';

class LoginRepository with ManageSessionData {
  final LoginApiService _loginApiService = LoginApiService();

  Future<ObjApiResponse> authenticateCredential(
          SessionCredential sessionCredential) =>
      _loginApiService.authenticateCredential(sessionCredential);

  Future<ObjApiResponse> validateUser(SessionUser sessionUser) =>
      _loginApiService.validateUser(sessionUser);

  Future<ObjApiResponse> olvideClave(SessionUser sessionUser) =>
      _loginApiService.olvideClave(sessionUser);
}
