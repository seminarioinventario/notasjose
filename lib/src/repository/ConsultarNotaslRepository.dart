import 'package:notas_uniajc/src/apiservices/ConsultarApiservice.dart';
import 'package:notas_uniajc/src/models/SessionNotas/ProgramaAcaModel.dart';
import '../models/SessionNotas/NotasActualesModel.dart';
import '../models/ObjApiResponse.dart';
import 'ManageSessionData.dart';
import '../models/session/UsuarioModel.dart';
import '../utils/ConstantsLabels.dart';

class ConsultarNotasRepository with ManageSessionData {
  ConsultarNotasRepository() {
    getValueLocalSotage(ConstantsLabels.keyToken).then((onvalue) {
      access = onvalue;
    });
  }
  final ConsultApiservice _consultApiservice = ConsultApiservice();

  Future<ObjApiResponse> notasActual(String pegeId) =>
      _consultApiservice.notasActual(pegeId, access);
  Future<ObjApiResponse> historicoNota(String pegeId) =>
      _consultApiservice.historicoNotas(pegeId, access);
  /*Future<ObjApiResponse> historicoNota(
          String pegeId, ProgramaAcademicoModel programaAcademicoModel) =>
      _consultApiservice.historicoNotas(pegeId, codigo);*/
  Future<ObjApiResponse> programaAcademico(String pegeId) =>
      _consultApiservice.programaAcademico(pegeId, access);
}
