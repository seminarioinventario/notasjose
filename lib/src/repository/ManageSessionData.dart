import 'package:notas_uniajc/src/utils/LocalObj_storage.dart';
import 'package:notas_uniajc/src/utils/session_storage.dart';

class ManageSessionData {
  final LocalObjStorage _localObjStorage = LocalObjStorage();
  final SessionStorage _sessionStorage = SessionStorage();

  String access;

  Future saveLocalDatatorage(String key, String value) =>
      _sessionStorage.writeValue(key, value);

  Future<String> getValueLocalSotage(String key) =>
      _sessionStorage.getValueforKey(key);
  Future deleteLocalSessionStorage() => _sessionStorage.deleteAllValues();

  Future saveObjStorage(String key, value) =>
      _localObjStorage.writeValue(key, value);

  Future<dynamic> getValueOBjforKey(String key) =>
      _localObjStorage.getValueforKey(key);

  Future deleteOBJValues(String key) => _localObjStorage.deleteValue(key);
  Future deleteAllValues() => _localObjStorage.deleteAllValues();
}
