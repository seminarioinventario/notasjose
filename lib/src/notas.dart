import 'package:notas_uniajc/src/ui/login/LoginPass_ui.dart';
import 'package:notas_uniajc/src/ui/login/login_ui.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:notas_uniajc/src/ui/splash_ui.dart';
import 'package:notas_uniajc/src/ui/home/home_ui.dart';

class Notas extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Notas Uniajc',
        themeMode: ThemeMode.light,
        // home: TabContainerIndexedStack(),
        darkTheme: ThemeData(
            brightness: Brightness.dark,
            textTheme:
                GoogleFonts.ralewayTextTheme(Theme.of(context).textTheme)),
        theme: //ThemeData.dark(),
            ThemeData(
                fontFamily: 'Raleway',
                visualDensity: VisualDensity.adaptivePlatformDensity,
                primarySwatch: Colors.blue,
                primaryColor: Color.fromARGB(255, 0, 108, 210),
                accentColor: Color.fromARGB(255, 0, 108, 210),
                textTheme:
                    GoogleFonts.ralewayTextTheme(Theme.of(context).textTheme)),
        initialRoute: "/",
        routes: {
          "/": (context) => SplashUi(),
          "/login_ui": (context) => LoginPage(),
          "/loginPass_ui": (context) => LoginPassPage(),
          "/home_ui": (context) => TabContainerIndexedStack(),
        },
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('en', 'US'), // English
          const Locale('es', 'ES'), // Español
          // ... other locales the app supports
        ]);
  }
}
