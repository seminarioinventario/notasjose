class SistemaCalific {
  String evalPeso;
  String articulo;

  SistemaCalific({this.evalPeso, this.articulo});

  factory SistemaCalific.fromJson(Map<String, dynamic> json) {
    return SistemaCalific(
      evalPeso: json['evalPeso'],
      articulo: json['articulo'],
    );
  }
}
