import 'CalificacionesModel.dart';

class MateriaModel {
  String codigoMateria;
  String descripMateria;
  String grupo;
  String nombreDocente;
  List<CalificacionesModel> calificaciones;

  MateriaModel({
    this.codigoMateria,
    this.descripMateria,
    this.grupo,
    this.nombreDocente,
    this.calificaciones,
  });

  factory MateriaModel.fromJson(Map<String, dynamic> json) {
    var list1 = json['materias'] as List;
    var p = 0;
    var listCalificaciones1 = list1[p]['calificaciones'] as List;

    List<CalificacionesModel> listaCalificaciones = List<CalificacionesModel>();

    //Meter esto en un for
    listaCalificaciones.add(new CalificacionesModel(
        evacDescripcion: listCalificaciones1[p]['evacDescripcion'],
        nota: listCalificaciones1[p]['nota'],
        peso: listCalificaciones1[p]['peso']));

    return MateriaModel(
      codigoMateria: list1[p]['codigoMateria'],
      descripMateria: list1[p]['descripMateria'],
      nombreDocente: list1[p]['nombreDocente'],
      calificaciones: listaCalificaciones,
    );
  }

  toList() {}
}
