class NotaxMateria {
  String descripcion;
  int noun;
  int peso;

  NotaxMateria({this.descripcion, this.noun, this.peso});

  factory NotaxMateria.fromJson(Map<String, dynamic> json) {
    return NotaxMateria(
      descripcion: json['descripcion'],
      noun: json['noun'],
      peso: json['peso'],
    );
  }
}
