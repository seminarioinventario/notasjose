import 'dart:ffi';

import 'NotasxMateriaModel.dart';
import 'SistemasCalifModel.dart';

class HistoricoNotaModel {
  String codigo;
  String docente;
  String grupo;
  String nombre;
  Float notaFinal;
  List<NotaxMateria> notas;
  String periodo;
  List<SistemaCalific> sistemadecalificacion;

  HistoricoNotaModel(
      {this.codigo,
      this.docente,
      this.grupo,
      this.nombre,
      this.notaFinal,
      this.notas,
      this.periodo,
      this.sistemadecalificacion});

  factory HistoricoNotaModel.fromJson(Map<String, dynamic> json) {
    var list = json['NotaxMateria'] as List;
    var list2 = json['SistemaCalific'] as List;

    return HistoricoNotaModel(
      codigo: json['codigo'],
      docente: json['docente'],
      grupo: json['grupo'],
      nombre: json['nombre'],
      notaFinal: json['notaFinal'],
      notas: list.map((i) => NotaxMateria.fromJson(i)).toList(),
      periodo: json['periodo'],
      sistemadecalificacion:
          list2.map((e) => SistemaCalific.fromJson(e)).toList(),
    );
  }
}
