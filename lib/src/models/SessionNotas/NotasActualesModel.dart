import 'dart:ffi';
import 'SistemasCalifModel.dart';
import 'MateriaModel.dart';

class NotasActualesModel {
  String codigoMateria;
  String descripMateria;
  //String nombreDocente;
  int semestre;
  List<MateriaModel> materiaModel;
  List<SistemaCalific> sistemaCalificacion;

  NotasActualesModel(
      {this.codigoMateria,
      this.descripMateria,
      //   this.nombreDocente,
      this.semestre,
      this.materiaModel,
      this.sistemaCalificacion});
  factory NotasActualesModel.fromJson(Map<String, dynamic> json) {
    var list = json['materiaModel'] as List;
    var list1 = json['sistemaCalificacion'] as List;
    return NotasActualesModel(
      codigoMateria: json['codigoMateria'],
      descripMateria: json['descripMateria'],
      // nombreDocente: json['nombreDocente'],
      semestre: json['semestre'],
      materiaModel: list.map((i) => MateriaModel.fromJson(i)).toList(),
      sistemaCalificacion:
          list1.map((e) => SistemaCalific.fromJson(e)).toList(),
    );
  }

  toList() {}
}
