class SistemaCalifAlt {
  String evalPeso;
  String articulo;
  String notaDescripcion;
  String notaPeso;

  SistemaCalifAlt(
      {this.evalPeso, this.articulo, this.notaDescripcion, this.notaPeso});
  factory SistemaCalifAlt.fromJson(Map<String, dynamic> json) {
    return SistemaCalifAlt(
      evalPeso: json['evalPeso'],
      articulo: json['articulo'],
      notaDescripcion: json['notaDescripcion'],
      notaPeso: json['notaPeso'],
    );
  }
}
