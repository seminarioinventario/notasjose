class SimpleObjectMessage {
  String codigo;
  String descripcion;
  String tipo;
  String ubicacion;

  SimpleObjectMessage(
      {this.codigo, this.descripcion, this.tipo, this.ubicacion});
  factory SimpleObjectMessage.fromJson(Map<String, dynamic> json) {
    return SimpleObjectMessage(
      codigo: json['codigo'],
      descripcion: json['descripcion'],
      tipo: json['tipo'],
      ubicacion: json['ubicacion'],
    );
  }
}
