class ProgramaAcademicoModel {
  String catDescripcion;
  String codigo;
  String nombre;
  String sitDescripcion;

  ProgramaAcademicoModel(
      {this.catDescripcion, this.codigo, this.nombre, this.sitDescripcion});

  factory ProgramaAcademicoModel.fromJson(Map<String, dynamic> json) {
    return ProgramaAcademicoModel(
      catDescripcion: json['catDescripcion'],
      codigo: json['codigo'],
      nombre: json['nombre'],
      sitDescripcion: json['sitDescripcion'],
    );
  }
}
