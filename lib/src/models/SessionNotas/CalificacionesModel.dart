import 'dart:ffi';

class CalificacionesModel {
  String evacDescripcion;
  double nota;
  int peso;

  CalificacionesModel({
    this.evacDescripcion,
    this.nota,
    this.peso,
  });
  factory CalificacionesModel.fromJson(Map<String, dynamic> json) {
    var list = json['calificaciones'] as List;
    var c = 0;

    //Meter esto en un for

    return CalificacionesModel(
      evacDescripcion: list[c], //Descripcion
      nota: list[c], //Nota
      peso: list[c], //Peso
    );
  }
  /*CalificacionesModel(List lista) {
    //var list = json['calificaciones'] as List;
    var c = 0;
    return CalificacionesModel(
      evacDescripcion: lista[c], //Descripcion
      nota: lista[c], //Nota
      peso: lista[c], //Peso
    );
  }*/
}
