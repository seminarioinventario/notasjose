class RolesModel{
  String vrolTipo;
  int vrolId;
  RolesModel({
    this.vrolTipo,
    this.vrolId
  });
  factory RolesModel.fromJson(Map<String, dynamic> json) {
    return RolesModel(
      vrolTipo: json['vrolTipo'],
      vrolId: json['vrolId']
    );
  }
}