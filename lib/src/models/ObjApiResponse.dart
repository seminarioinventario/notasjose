class ObjApiResponse {
  int codigo;
  String mensaje;
  Object valor;
  ObjApiResponse({this.codigo, this.mensaje, this.valor});
  factory ObjApiResponse.fromJson(Map<String, dynamic> json) {
    return ObjApiResponse(
      codigo: json['codigo'],
      mensaje: json['mensaje'],
      valor: json['valor'],
    );
  }

  Map<String, dynamic> toJson() =>
      {'codigo': codigo, 'mensaje': mensaje, 'valor': mensaje};
}
