class RolModel {
  int vrolId;
  String vrolNombre;
  String vrolDescripcion;
  String vrolTipo;
  String vrolEstado;
  String vrolPublico;

  RolModel(
      {this.vrolId,
      this.vrolNombre,
      this.vrolDescripcion,
      this.vrolTipo,
      this.vrolEstado,
      this.vrolPublico});

  factory RolModel.fromJson(Map<String, dynamic> json) {
    return RolModel(
        vrolId: json['vrolId'],
        vrolNombre: json['vrolNombre'],
        vrolDescripcion: json['vrolDescripcion'],
        vrolTipo: json['vrolTipo'],
        vrolEstado: json['vrolEstado'],
        vrolPublico: json['vrolPublico']);
  }
}
