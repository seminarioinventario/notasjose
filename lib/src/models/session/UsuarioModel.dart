import 'package:notas_uniajc/src/models/session/RolModel.dart';

class UsuarioModel {
  int idUsuario;
  String identificacion;
  String correoElectronico;
  int pegeId;
  String estado;
  String tokenAccess;
  String usuaFechacambio;
  String primernombre;
  String segundonombre;
  String primerapellido;
  String segundoapellido;
  List<RolModel> listadoroles;

  UsuarioModel(
      {this.idUsuario,
      this.identificacion,
      this.correoElectronico,
      this.pegeId,
      this.estado,
      this.tokenAccess,
      this.usuaFechacambio,
      this.primernombre,
      this.segundonombre,
      this.primerapellido,
      this.segundoapellido,
      this.listadoroles});

  factory UsuarioModel.fromJson(Map<String, dynamic> json) {
    var listadoroles = json['listadoroles'] as List;

    return UsuarioModel(
      idUsuario: json['idUsuario'],
      identificacion: json['identificacion'],
      correoElectronico: json['correoElectronico'],
      pegeId: json['pegeId'],
      estado: json['estado'],
      tokenAccess: json['tokenAcces'],
      usuaFechacambio: json['usuaFechacambio'],
      primernombre: json['primernombre'],
      segundonombre: json['segundonombre'],
      primerapellido: json['primerapellido'],
      segundoapellido: json['segundoapellido'],
      listadoroles: listadoroles.map((i) => RolModel.fromJson(i)).toList(),
    );
  }

  String getNombre() {
    return capitalize(primernombre) + " " + capitalize(primerapellido);
  }

  String capitalize(String string) {
    if (string == null) {
      throw ArgumentError("string: $string");
    }

    if (string.isEmpty) {
      return string;
    }

    return string[0].toUpperCase() + string.substring(1).toLowerCase();
  }
}
