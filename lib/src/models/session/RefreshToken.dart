class RefreshToken {
  String login;
  String token;
  bool rememberSession;
  RefreshToken({this.login, this.token, this.rememberSession});
  Map<String, dynamic> toJson() =>
      {'login': login, 'token': token, 'rememberSession': rememberSession};
}
