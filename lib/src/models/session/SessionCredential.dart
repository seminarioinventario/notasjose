class SessionCredential {
  String login;
  String password;
  String rememberSession;
  SessionCredential({this.login, this.password});
  Map<String, dynamic> toJson() => {'login': login, 'password': password};
}
