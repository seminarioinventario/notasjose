import 'package:notas_uniajc/src/models/RolesModel.dart';

class UsuarioModel {
  int idUsuario;
  String login;
  String identificacion;
  String correoelectronico;
  int pegeId;
  String estado;
  String token;
  String primernombre;
  String segundonombre;
  String primerapellido;
  String segundoapellido;
  List<RolesModel> listRoles;

  UsuarioModel(
  {
    this.idUsuario,  
    this.login,
    this.identificacion,
    this.correoelectronico,
    this.pegeId,
    this.estado,
    this.token,
    this.primernombre,
    this.segundonombre,
    this.primerapellido,
    this.segundoapellido,
    this.listRoles
  });

  factory UsuarioModel.fromJson(Map<String, dynamic> json) {
    var list = json['listRoles'] as List;

    return UsuarioModel(
      idUsuario: json['idUsuario'],
      login: json['login'],
      identificacion: json['identificacion'],
      correoelectronico: json['correoElectronico'],
      pegeId: json['pegeId'],
      estado: json['estado'],
      token: json['tokenAccess'],
      primernombre: json['primernombre'],
      segundonombre: json['segundonombre'],
      primerapellido: json['primerapellido'],
      segundoapellido: json['segundoapellido'],
      listRoles: list.map((index) => RolesModel.fromJson(index)).toList()
    );
  }
}
