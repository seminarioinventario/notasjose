import 'package:flutter/material.dart';
import 'package:notas_uniajc/src/Bloc/ConsultarNotasBloc.dart';

import 'homePage/nota_ui.dart';
import 'homePage/historico_ui.dart';
import 'homePage/configuracion_ui.dart';

class TabContainerIndexedStack extends StatefulWidget {
  TabContainerIndexedStack({Key key}) : super(key: key);

  @override
  _TabContainerIndexedStackState createState() =>
      _TabContainerIndexedStackState();
}

class _TabContainerIndexedStackState extends State<TabContainerIndexedStack> {
  int tabIndex = 0;
  ConsultarNotasBloc consultarNotasBloc;
  List<Widget> listScreens;
  @override
  void initState() {
    consultarNotasBloc = ConsultarNotasBloc();
    super.initState();
    listScreens = [
      Tab1(),
      Tab2(),
      Tab3(),
    ];
  }

//  @override
//  bool get wantKeepAlive =>
//      true; //by default it will be null, change it to true.

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      color: Colors.yellow,
      home: Scaffold(
        body: IndexedStack(index: tabIndex, children: listScreens),
        bottomNavigationBar: BottomNavigationBar(
            currentIndex: tabIndex,
            onTap: (int index) {
              setState(() {
                tabIndex = index;
              });
            },
            items: [
              BottomNavigationBarItem(
                icon: Icon(Icons.book),
                title: Text('Notas'),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.history),
                title: Text('Historico'),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.settings),
                title: Text('Configuracio'),
              ),
            ]),
        backgroundColor: Theme.of(context).primaryColor,
      ),
    );
  }
}
