import 'package:expansion_card/expansion_card.dart';
import 'package:flutter/material.dart';
import 'package:notas_uniajc/src/Bloc/ConsultarNotasBloc.dart';
import 'package:notas_uniajc/src/models/SessionNotas/HistoricoNotasModel.dart';
import 'package:notas_uniajc/src/models/SessionNotas/ProgramaAcaModel.dart';
import 'package:notas_uniajc/src/utils/CommonsWidget.dart';

class Tab2 extends StatefulWidget {
  @override
  _Tab2State createState() => _Tab2State();
}

class _Tab2State extends State<Tab2> with AutomaticKeepAliveClientMixin<Tab2> {
  ConsultarNotasBloc consultarNotasBloc;
  TextEditingController editingController = TextEditingController();
  //ProgramaAcademicoModel programaAcademicoModel;
  @override
  void initState() {
    super.initState();
    consultarNotasBloc = ConsultarNotasBloc();
    //  programaAcademicoModel = ProgramaAcademicoModel();
    //consultarNotasBloc.historicoNotas();
  }

  @override
  Widget build(BuildContext context) {
    consultarNotasBloc.programaAcademico(context);
    final textTheme = Theme.of(context).textTheme;
    print('build Historico');
    return Scaffold(
      appBar: AppBar(
        title: Text('Historico'),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: StreamBuilder(
              stream: consultarNotasBloc.list1,
              initialData: consultarNotasBloc.getInitalList1,
              builder: (BuildContext contex, snapData) {
                if (!snapData.hasData) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
                return CommonsWidget().cargaListaPrograma(snapData.data,
                    consultarNotasBloc, editingController, context, textTheme);
              },
            ),
          )
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
