import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:notas_uniajc/src/Bloc/LoginBloc.dart';
import 'package:notas_uniajc/src/utils/ConstantsImages.dart';
import 'package:notas_uniajc/src/utils/ConstantsLabels.dart';
import 'package:notas_uniajc/src/utils/RouteNavigator.dart';

class Tab3 extends StatefulWidget {
  Tab3({Key key}) : super(key: key);

  @override
  _Tab3State createState() => _Tab3State();
}

class _Tab3State extends State<Tab3> with AutomaticKeepAliveClientMixin<Tab3> {
  LoginBloc loginBloc;
  String nombre = "";
  //LoginApiService _loginRepository;

  @override
  void initState() {
    super.initState();
    loginBloc = LoginBloc();
    loginBloc.getName().then((value) => {
          setState(() {
            nombre = value;
          })
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Configuracion'),
      ),
      body: ListView(
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.edit, color: Colors.blue),
            title: Text('personalizar'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.info_outline, color: Colors.blue),
            title: Text('Acerca de'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app, color: Colors.blue),
            title: Text('Cerrar sesion'),
            onTap: () {
              loginBloc.closeSession(context, ConstantsLabels.msgCerrarSesion);
            },
          ),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
