import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:expansion_card/expansion_card.dart';
import 'package:notas_uniajc/src/models/ObjApiResponse.dart';
import 'package:notas_uniajc/src/models/SessionNotas/NotasActualesModel.dart';
import 'package:notas_uniajc/src/Bloc/ConsultarNotasBloc.dart';
import 'package:notas_uniajc/src/models/SessionNotas/NotasActualesModel.dart';
import 'package:notas_uniajc/src/apiservices/ConsultarApiservice.dart';
import 'package:notas_uniajc/src/utils/CommonsWidget.dart';
import 'package:notas_uniajc/src/utils/ConstantsLabels.dart';

class Tab1 extends StatefulWidget {
  @override
  _Tab1State createState() => _Tab1State();
}

class _Tab1State extends State<Tab1> with AutomaticKeepAliveClientMixin<Tab1> {
  ConsultarNotasBloc consultarNotasBloc;
  TextEditingController editingController = TextEditingController();
//  NotasActualesModel notasActualesModel;
  @override
  void initState() {
    super.initState();

    print('initState Nota');
    consultarNotasBloc = ConsultarNotasBloc();
    //  notasActualesModel = NotasActualesModel();
  }

  @override
  Widget build(BuildContext context) {
    consultarNotasBloc.notasActules(context);
    final textTheme = Theme.of(context).textTheme;
    print('build Nota');
    // print(notasActualesModel.grupo + "" + notasActualesModel.codigoMateria);
    return Scaffold(
      appBar: AppBar(
        title: Text('Nota'),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: StreamBuilder(
              stream: consultarNotasBloc.list,
              initialData: consultarNotasBloc.getInitalList,
              builder: (BuildContext contex, snapData1) {
                if (!snapData1.hasData) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
                return CommonsWidget().cargaLista(snapData1.data,
                    consultarNotasBloc, editingController, context, textTheme);
              },
            ),
          )
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
