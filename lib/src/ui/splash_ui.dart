import 'dart:async';

import 'package:notas_uniajc/src/bloc/LoginBloc.dart';
import 'package:notas_uniajc/src/utils/RouteNavigator.dart';
import 'package:flutter/material.dart';

class SplashUi extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashUi> {
  LoginBloc _bloc;
  @override
  void initState() {
    _bloc = LoginBloc();
    super.initState();
    Timer(Duration(seconds: 3), () => RouteNavigator.goToLoginUser(context));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Center(
              child: Container(
                margin: const EdgeInsets.fromLTRB(80, 0, 80, 0),
                child: Image.asset('lib/src/resources/images/img-Login.png'),
              ),
            ),
            Positioned(
              child: Align(
                  alignment: FractionalOffset.bottomCenter,
                  child: Image.asset(
                    'lib/src/resources/images/logosmartcampusv2.png',
                    width: 200,
                    height: 100,
                  )),
            )
          ],
        ),
      ),
    );
  }
}
