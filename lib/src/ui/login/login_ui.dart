import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:notas_uniajc/src/Bloc/LoginBloc.dart';
import 'package:notas_uniajc/src/ui/login/LoginPass_ui.dart';
//import 'package:notas_uniajc/src/ui/login/password_field.dart';
import 'package:wc_form_validators/wc_form_validators.dart';
import 'package:notas_uniajc/src/utils/CommonsWidget.dart';
import 'package:notas_uniajc/src/utils/ConstantsLabels.dart';
import 'package:notas_uniajc/src/utils/RouteNavigator.dart';
import 'package:notas_uniajc/src/utils/ConstantsImages.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({
    Key key,
  }) : super(key: key);

  @override
  State createState() => LoginPageState();
}

class UseData {
  String usuario = '';
  // String password = '';
}

class LoginPageState extends State<LoginPage>
    with SingleTickerProviderStateMixin {
  UseData user = UseData();
  bool formedited = false;
  bool _autovalidate = false;
  final GlobalKey<FormState> _formKey1 = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  //final GlobalKey<FormFieldState<String>> _passwordFieldKey =
  //  GlobalKey<FormFieldState<String>>();
  //final String usuario;
  LoginBloc loginBloc;
//  LoginPageState({});

  @override
  void initState() {
    super.initState();
    loginBloc = LoginBloc();
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
    ));
  }

  void gotoPasswordUi(context) {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (BuildContext contex) => LoginPassPage(
        usuario: user.usuario,
      ),
    ));
  }

  void _handleSubmitted() {
    final FormState form = _formKey1.currentState;
    if (!form.validate()) {
      _autovalidate = true;
      showInSnackBar("Por favor corregir el campo");
    } else {
      form.save();
      loginBloc.validateUser(user.usuario, context).then((value) {
        print(user.usuario);
        switch (value.codigo) {
          case 200:
            gotoPasswordUi(context);
            break;
          case 201:
            CommonsWidget().showAlertDialogOneButton(
                context, ConstantsLabels.atencionLabel, " ");
            break;
          case 401:
            CommonsWidget().showAlertDialogOneButton(
                context, ConstantsLabels.atencionLabel, " ");
            break;
          case 403:
            CommonsWidget().showAlertDialogOneButton(
                context, ConstantsLabels.atencionLabel, "");
            break;
          case 404:
            CommonsWidget().showAlertDialogOneButton(
                context, ConstantsLabels.atencionLabel, "");
            break;
          case 500:
            CommonsWidget().showAlertDialogOneButton(
                context, ConstantsLabels.atencionLabel, " ");
            break;

          default:
            CommonsWidget().showAlertDialogOneButton(
                context,
                ConstantsLabels.atencionLabel,
                "Ha ocurrido un error, por favor contacta a calidad.smartcampus.uniajc@gmail.com");
        }
      });
    }
  }

  /*   loginBloc
          .validarCredenciales(user.usuario, user.password, context)
          .then((value) {
        switch (value.codigo) {
          case 200:
            /* CommonsWidget().showAlertDialogOneButton1(
                context,
                ConstantsLabels.atencionLabel,
                "No hay un valor especificado para el servicio",
                false);*/

            if (value.valor) {
              RouteNavigator.goToHome(context);
            } else {
              RouteNavigator.goToLoginUser(context);
            }
            break;
          case 401:
            CommonsWidget().showAlertDialogOneButton(
                context, ConstantsLabels.atencionLabel, value.mensaje);
            break;
          case 300:
            CommonsWidget().showAlertDialogOneButton(
                context, ConstantsLabels.atencionLabel, value.mensaje);
            break;
          default:
            CommonsWidget().showAlertDialogOneButton(
                context,
                ConstantsLabels.atencionLabel,
                "Ha ocurrido un error, por favor contacta a calidad.smartcampus.uniajc@gmail.com");
        }
      });
    }
  }*/

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    double heightt = MediaQuery.of(context).size.height;
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      body: SafeArea(
        child: LayoutBuilder(builder: (context, constraints) {
          return SingleChildScrollView(
            child: ConstrainedBox(
              constraints: BoxConstraints(minHeight: constraints.maxHeight),
              child: IntrinsicHeight(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: 25),
                      child: Center(
                          child: Image.asset(
                        'lib/src/resources/images/Capa 2xxhdpi.png',
                        width: 200,
                        height: 120,
                      )),
                    ),
                    Container(
                      padding:
                          EdgeInsets.only(top: 0.0, left: 20.0, right: 20.0),
                      child: Form(
                        key: _formKey1,
                        autovalidate: _autovalidate,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Container(
                                padding: EdgeInsets.only(
                                    top: 20.0, left: 0, bottom: 0),
                                child: Row(
                                  children: <Widget>[
                                    Image(
                                      image: AssetImage(
                                          'lib/src/resources/images/logogris.png'),
                                      width: 50,
                                      height: 50,
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          "Bienvenido",
                                          style: textTheme.headline4.copyWith(
                                              color: ConstantsImages
                                                  .colorAzulUniajc),
                                        ),
                                        Text(
                                          "#UniCamacho",
                                          style: textTheme.bodyText2,
                                        )
                                      ],
                                    )
                                  ],
                                )),
                            SizedBox(height: heightt * 0.06),
                            TextFormField(
                              toolbarOptions: ToolbarOptions(
                                  copy: true,
                                  cut: false,
                                  paste: true,
                                  selectAll: true),
                              decoration: InputDecoration(
                                  labelText: "Usuario",
                                  hintText: "",
                                  helperText: "Digite usuario de Academusoft",
                                  icon: Icon(
                                    Icons.account_circle,
                                  )),
                              keyboardType: TextInputType.text,
                              onSaved: (String value) {
                                user.usuario = value;
                              },
                              validator: Validators.compose(
                                  [Validators.required("Campo Requerido")]),
                              style: textTheme.bodyText1,
                            ),
                            /*SizedBox(height: heightt * 0.06),
                            PasswordField(
                              fieldKey: _passwordFieldKey,
                              helperText: "Contraseña de Smart Campus",
                              hintText: "",
                              labelText: "Contraseña",
                              onSaved: (String value) {
                                user.password = value;
                              },
                              validator: Validators.compose([
                                Validators.required("Contraseña requerida")
                              ]),
                            ),*/
                            SizedBox(height: heightt * 0.06),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                /*  FractionallySizedBox(
                                  child: ButtonTheme(
                                    height: 40.0,
                                    minWidth: 130.0,
                                    child: OutlineButton(
                                      child: Text("Cancelar",
                                          style: textTheme.button.copyWith(
                                              color: Color.fromARGB(
                                                  255, 0, 108, 210))),
                                      shape: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(20.0)),
                                      ),
                                      borderSide: BorderSide(
                                          color:
                                              Color.fromARGB(255, 0, 108, 210),
                                          style: BorderStyle.solid,
                                          width: 1),
                                      onPressed: () {
                                        Navigator.of(context).pop(true);
                                      },
                                    ),
                                  ),
                                ),*/
                                FractionallySizedBox(
                                  child: MaterialButton(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(20.0)),
                                    ),
                                    height: 40.0,
                                    minWidth: 130.0,
                                    color: Color.fromARGB(255, 0, 108, 210),
                                    textColor: Colors.white,
                                    child: Text("Continuar",
                                        style: textTheme.button
                                            .copyWith(color: Colors.white)),
                                    onPressed: _handleSubmitted,
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 30.0),
                            const SizedBox(height: 15.0),
                          ],
                        ),
                      ),
                    ),
                    Center(
                      child: Container(
                        padding: EdgeInsets.only(bottom: 25),
                        child: Image.asset(
                          'lib/src/resources/images/logosmartcampusv2.png',
                          width: 200,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        }),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
