import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:notas_uniajc/src/apiservices/ConsultarApiservice.dart';
import 'package:notas_uniajc/src/repository/ConsultarNotaslRepository.dart';
import 'package:notas_uniajc/src/models/session/UsuarioModel.dart';
import 'package:notas_uniajc/src/utils/ConstantsImages.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../models/ObjApiResponse.dart';
import 'package:progress_dialog/progress_dialog.dart';
import '../models/session/SessionCredential.dart';
import '../repository/LoginRepository.dart';
import '../utils/RouteNavigator.dart';
import '../utils/CommonsWidget.dart';
import '../utils/ConstantsLabels.dart';
import '../models/session/SessionUser.dart';
import 'package:notas_uniajc/src/utils/ConstantsLabels.dart';

class LoginBloc {
  final _loginApiService = LoginRepository();
  ProgressDialog _progressDialog;
  UsuarioModel usuarioModel;
  LoginBloc();

  Future<ObjApiResponse> validateUser(String user, context) async {
    _progressDialog = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    _progressDialog.style(message: "Validando..");
    await _progressDialog.show();
    SessionUser sessionUser = SessionUser(username: user);
    ObjApiResponse apiResponse =
        await _loginApiService.validateUser(sessionUser);

    await _progressDialog.hide();

    return apiResponse;
  }

  Future<ObjApiResponse> olvideClave(String user, context) async {
    SessionUser sessionUser = SessionUser(username: user);
    _progressDialog = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    _progressDialog.style(message: "Validando..");
    await _progressDialog.show();
    ObjApiResponse apiResponse =
        await _loginApiService.olvideClave(sessionUser);
    await _progressDialog.hide();
    return apiResponse;
  }

  Future<bool> closeSession(BuildContext context, String message) async {
    return await showDialog<bool>(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              shape: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(20.0)),
              ),
              title: Center(
                  child: Column(
                children: <Widget>[
                  Icon(
                    Icons.exit_to_app,
                    color: ConstantsImages.colorAzulUniajc,
                    size: 60,
                  ),
                  const SizedBox(height: 10.0),
                  Text("Atención",
                      style: GoogleFonts.raleway(
                          fontWeight: FontWeight.bold, fontSize: 20)),
                ],
              )),
              content: Container(
                width: double.maxFinite,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Center(child: Text(message)),
                      const SizedBox(height: 30.0),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          ButtonTheme(
                            minWidth: 100.0,
                            child: OutlineButton(
                                child: Text(
                                  ConstantsLabels.noLabe,
                                  style: TextStyle(
                                      fontSize: 14.0,
                                      color: ConstantsImages.colorAzulUniajc),
                                ),
                                shape: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20.0)),
                                ),
                                borderSide: BorderSide(
                                    color: ConstantsImages.colorAzulUniajc,
                                    style: BorderStyle.solid,
                                    width: 1),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                }),
                          ),
                          SizedBox(
                            width: 100.0,
                            child: RawMaterialButton(
                              shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20.0)),
                              ),
                              fillColor: ConstantsImages.colorAzulUniajc,
                              splashColor: Colors.blueAccent,
                              child: Text(
                                ConstantsLabels.siLabel,
                                style:
                                    Theme.of(context).textTheme.button.copyWith(
                                          color: Colors.white,
                                        ),
                              ),
                              onPressed: () {
                                Navigator.of(context).pop();
                                _loginApiService.deleteLocalSessionStorage();
                                _loginApiService.deleteAllValues();
                                RouteNavigator.goToLoginUser(context);
                              },
                            ),
                          ),
                        ],
                      ),
                    ]),
              ),
            );
          },
        ) ??
        false;
  }

  Future<String> getName() async {
    String nombre =
        await _loginApiService.getValueLocalSotage(ConstantsLabels.keyName);

    return nombre;
  }

  Future<ObjApiResponse> validarCredenciales(
      String user, String clave, context) async {
    _progressDialog = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    _progressDialog.style(message: "Validando..");
    await _progressDialog.show();
    SessionCredential sessionCredential =
        SessionCredential(login: user, password: clave);
    ObjApiResponse apiResponse =
        await _loginApiService.authenticateCredential(sessionCredential);
    if (apiResponse.codigo == 200) {
      UsuarioModel usuarioModel = UsuarioModel.fromJson(apiResponse.valor);
      //  UsuarioData _pegeId = usuarioModel.pegeId.toString() as UsuarioData;
      print("token en el Login: " + usuarioModel.tokenAccess);

      final _pegeId = await SharedPreferences.getInstance();

// fijar valorf
      _pegeId.setString('pegeId', usuarioModel.pegeId.toString());

      final _tokenAccess = await SharedPreferences.getInstance();

// fijar valorf
      _tokenAccess.setString('tokenAccess', usuarioModel.tokenAccess);
      // if (usuarioModel.listadoroles
      //   .where((element) => element.vrolTipo.contains("USUARIO"))
      // .isNotEmpty) {

      // await _loginApiService.saveLocalDatatorage(ConstantsLabels.keyPegeId, usuarioModel.pegeId.toString());

      //await _loginApiService.saveLocalDatatorage(   ConstantsLabels.keyToken, usuarioModel.tokenAcces);

      //await _loginApiService.saveLocalDatatorage( ConstantsLabels.keyName, usuarioModel.getNombre());
      //}
      //String peId =
      //  await _loginApiService.getValueLocalSotage(ConstantsLabels.keyPegeId);
      //print("guardando");
      //print(peId);

      // obtener preferencias compartidas
      print(apiResponse.valor);
      //print(usuarioModel.pegeId);
      //final pegeId = await SharedPreferences.getInstance();

// fijar valorf
      //pegeId.setString('pegeId', usuarioModel.pegeId.toString());
      //var _elpegeId = (pegeId.getString('pegeId') ?? 0);
      //print("mi pegeid:");
      //print(_elpegeId);
      //print(pegeId.toString() + " pegeid");
// Intenta leer datos de la clave del contador. Si no existe, retorna 0.
      //pegeId = prefs.getInt('pegeId') ?? 0;

      // final pres = await SharedPreferences.getInstance();

// fijar valor
      //pres.setInt('pegeId', usuarioModel.pegeId);
      // ObjApiResponse notasResponse =
      //   await _consultApiservice.notasActual(usuarioModel.pegeId.toString());
      //print("JJJJJJJJJJJJ " + notasResponse.valor.toString() + " SSSSSSS");

      RouteNavigator.goToHome(context);
    }
    if (apiResponse.codigo == 201) {
      CommonsWidget().showAlertDialogOneButton1(
          context, ConstantsLabels.atencionLabel, "", false);
    }
    if (apiResponse.codigo == 401) {
      CommonsWidget().showAlertDialogOneButton1(
          context,
          ConstantsLabels.atencionLabel,
          "El usuario o la contraseña es incorrectas",
          false);
    }
    if (apiResponse.codigo == 403) {
      CommonsWidget().showAlertDialogOneButton1(
          context,
          ConstantsLabels.atencionLabel,
          "No hay un valor especificado para el servicio",
          false);
    }
    if (apiResponse.codigo == 404) {
      CommonsWidget().showAlertDialogOneButton1(
          context,
          ConstantsLabels.atencionLabel,
          "No hay un valor especificado para el servicio",
          false);
    }
    if (apiResponse.codigo == 500) {
      CommonsWidget().showAlertDialogOneButton1(
          context,
          ConstantsLabels.atencionLabel,
          "No hay un valor especificado para el servicio",
          false);
    }
    return apiResponse;
    /* Future<bool> getAccesToken() async {
    bool tokenExist = false;
    String token =
        await _loginApiService.getValueLocalSotage(ConstantsLabels.keyToken);
    if (token != null) {
      tokenExist = true;
    }
    return tokenExist;
  }*/

    /* Future<bool> authenticateToken() async {
    bool validate = false;
    String token =
        await _loginApiService.getValueLocalSotage(ConstantsLabels.keyToken);
    String obj = await _loginApiService
        .getValueLocalSotage(ConstantsLabels.keyIdObj);
    if (token != null && obj != null) {
      SessionToken sessionToken = SessionToken(token: token);
      ObjApiResponse apiResponse =
          await _loginApiService.authenticateToken(sessionToken);
      switch (apiResponse.codigo) {
        case 200:
          ObjModel objModel = ObjModel.fromJson(apiResponse.valor);
          validate = objModel.estado == "ACTIVO";
          break;
        case ConstantsLabels.codeErrorInternet:
          validate = true;
          break;
        case ConstantsLabels.codeErrorApi:
          validate = true;
          break;
        default:
          validate = false;
      }
    }
    return validate;
  }*/

    /*Future<ObjApiResponse> validateUser(String user, context) async {
    _progressDialog = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    _progressDialog.style(message: "Validando..");
    await _progressDialog.show();
    SessionUser sessionUser = SessionUser(username: user);
    ObjApiResponse apiResponse =
        await _loginApiService.validateUser(sessionUser);
    await _progressDialog.hide();

    return apiResponse;
  }*/

    /*Future<ObjApiResponse> olvideClave(String user, context) async {
      SessionUser sessionUser = SessionUser(username: user);
      _progressDialog = ProgressDialog(context,
          type: ProgressDialogType.Normal,
          isDismissible: false,
          showLogs: true);
      _progressDialog.style(message: "Validando..");
      await _progressDialog.show();
      ObjApiResponse apiResponse =
          await _loginApiService.olvideClave(sessionUser);
      await _progressDialog.hide();
      return apiResponse;
    }*/

    /* Future<String> refreshToken() async {
    String user = await _usuarioRepository
        .getValueLocalSotage(ConstantsLabels.keyUserName);
    String token = await _usuarioRepository
        .getValueLocalSotage(ConstantsLabels.keyToken);
    ObjApiResponse apiResponse = await _loginApiService.refreshToken(
        RefreshToken(login: user, token: token, rememberSession: true));
      switch (apiResponse.codigo) {
        case 200:

          break;
        default:
      }
  }*/
  }
}
