import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:notas_uniajc/src/models/SessionNotas/NotasActualesModel.dart';
import 'package:notas_uniajc/src/models/session/UsuarioModel.dart';
import 'package:notas_uniajc/src/utils/ConstantsLabels.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../models/ObjApiResponse.dart';
import '../models/SessionNotas/HistoricoNotasModel.dart';
import '../models/SessionNotas/ProgramaAcaModel.dart';
import '../repository/ConsultarNotaslRepository.dart';
import '../apiservices/ConsultarApiservice.dart';
import '../models/SessionNotas/ProgramaAcaModel.dart';
import '../models/SessionNotas/NotasActualModel.dart';
import '../models/SessionNotas/MateriaModel.dart';
import '../models/SessionNotas/CalificacionesModel.dart';
import '../models/SessionNotas/SistemasCalifModel.dart';

class ConsultarNotasBloc {
  final ConsultarNotasRepository _consultarNotasRepository =
      ConsultarNotasRepository();

  //final historicoNotasModel = HistoricoNotaModel();
  //final programaModel = ProgramaModel();
  final _list = StreamController<List<ProgramaModel>>.broadcast();
  List<ProgramaModel> _initialList;
  List<ProgramaModel> get getInitalList => _initialList;
  Stream<List<ProgramaModel>> get list => _list.stream.asBroadcastStream();

  /*final _listMateria = StreamController<List<MateriaModel>>.broadcast();
  List<MateriaModel> _initialListMateria;
  List<MateriaModel> get getInitalListMateria => _initialListMateria;
  Stream<List<MateriaModel>> get listMateria =>
      _listMateria.stream.asBroadcastStream();

  final _listCalificaciones =
      StreamController<List<CalificacionesModel>>.broadcast();
  List<CalificacionesModel> _initialListCalificaciones;
  List<CalificacionesModel> get getInitalListCalificaciones =>
      _initialListCalificaciones;
  Stream<List<CalificacionesModel>> get listCalificaciones =>
      _listCalificaciones.stream.asBroadcastStream();

  final _list2 = StreamController<List<SistemaCalific>>.broadcast();
  List<SistemaCalific> _initialList2;
  List<SistemaCalific> get getInitalList2 => _initialList2;
  Stream<List<SistemaCalific>> get list2 => _list2.stream.asBroadcastStream();
*/
  final _list1 = StreamController<List<ProgramaAcademicoModel>>.broadcast();
  List<ProgramaAcademicoModel> _initialList1;
  List<ProgramaAcademicoModel> get getInitalList1 => _initialList1;
  Stream<List<ProgramaAcademicoModel>> get list1 =>
      _list1.stream.asBroadcastStream();

  final _consultApiservice = ConsultApiservice();
  // final _programaAcademicoModel= ProgramaAcademicoModel();
  final programaModel = ProgramaModel();
  ConsultarNotasBloc();
  Future<ObjApiResponse> historicoNotas(context) async {
    String pegeId = await _consultarNotasRepository
        .getValueLocalSotage(ConstantsLabels.keyPegeId);
    var programaAcademicoModel;

    ProgramaAcademicoModel codigoAcademico =
        await programaAcademicoModel.codigo;
    print(pegeId);
    ObjApiResponse apiResponse =
        await _consultarNotasRepository.historicoNota(pegeId);
    if (apiResponse.codigo == 200) {
      print(apiResponse.valor);
    } else if (apiResponse.codigo == 400) {
    } else if (apiResponse.codigo == 401) {
    } else if (apiResponse.codigo == 403) {
    } else if (apiResponse.codigo == 404) {
    } else if (apiResponse.codigo == 500) {}

    return apiResponse;
  }

  /*Future<ObjApiResponse> historicoNotas(context) async {
    String pegeId = await _consultarNotasRepository
        .getValueLocalSotage(ConstantsLabels.keyPegeId);
    var programaAcademicoModel;

    //ProgramaAcademicoModel codigo = await programaAcademicoModel.codigo;
    ObjApiResponse apiResponseProgramaAcademico =
        await _consultarNotasRepository.programaAcademico(pegeId);
    if (apiResponseProgramaAcademico.codigo == 200) {
       
      print(apiResponseProgramaAcademico.valor);
      programaAcademicoModel['codigo']
          .forEach((apiResponseProgramaAcademico) {
        final apiResponseProgramaAcademicoTemp = apiResponseProgramaAcademico;
        apiResponseProgramaAcademico.add(apiResponseProgramaAcademicoTemp);
      });

      ObjApiResponse apiResponseHistoNotas = await _consultarNotasRepository
          .historicoNota(pegeId, apiResponseProgramaAcademico.valor.);
      if (apiResponseHistoNotas.codigo == 200) {
        print(apiResponseHistoNotas.valor);
      } else if (apiResponseHistoNotas.codigo == 400) {
      } else if (apiResponseHistoNotas.codigo == 401) {
      } else if (apiResponseHistoNotas.codigo == 403) {
      } else if (apiResponseHistoNotas.codigo == 404) {
      } else if (apiResponseHistoNotas.codigo == 500) {}
    }
    
    return apiResponseHistoNotas;
  }
*/

  Future<ObjApiResponse> notasActules(context) async {
    SharedPreferences elpegeId = await SharedPreferences.getInstance();
    SharedPreferences eltokenAccess = await SharedPreferences.getInstance();

    var pegeId = (elpegeId.getString('pegeId') ?? 0);
    var tokenAccess = (eltokenAccess.getString('tokenAccess') ?? 0);
    ObjApiResponse apiResponse =
        await _consultApiservice.notasActual(pegeId, tokenAccess);

    if (apiResponse.codigo == 200) {
      /*  print("Nombre de programa");
      print(apiResponse.valor);
      ProgramaModel programaModel = ProgramaModel.fromJson(apiResponse.valor);
      print("Nombre de programa------2" + programaModel.toString());
       pro
      print(programaModel.nombrePrograma);*/
      //programaModel.forEach((programaModel) => programaModel.interview());
      print("Bloc notas 129");
      print(apiResponse.valor);

      _initialList = (apiResponse.valor as List)
          .map((e) => new ProgramaModel.fromJson(e))
          .toList();

      /*_listMateria.add(_initialListMateria);
      _initialListMateria = (apiResponse.valor as List)
          .map((l) => new MateriaModel.fromJson(l))
          .toList();

      _initialListCalificaciones = (apiResponse.valor as List)
          .map((j) => new CalificacionesModel.fromJson(j))
          .toList();

      _initialList2 = (apiResponse.valor as List)
          .map((i) => new SistemaCalific.fromJson(i))
          .toList();*/

      _list.add(_initialList);

      /* _listMateria.add(_initialListMateria);

      _listCalificaciones.add(_initialListCalificaciones);

      _list2.add(_initialList2);*/
      print(_list);
    } else {
      _initialList = List<ProgramaModel>();
      _list.add(_initialList);

      print("i");
    }
    if (apiResponse.codigo == 400) {
    } else if (apiResponse.codigo == 401) {
    } else if (apiResponse.codigo == 403) {
    } else if (apiResponse.codigo == 404) {
    } else if (apiResponse.codigo == 500) {}

    return apiResponse;
  }

//---------------------------------------------//
  /*Future notasActual(context) async {
    String pegeId = await _consultarNotasRepository
        .getValueLocalSotage(ConstantsLabels.keyPegeId);
    ObjApiResponse apiResponse =
        await _consultarNotasRepository.notasActual(pegeId);
    print("error de la consulta notas" + apiResponse.valor.toString());
    if (apiResponse.codigo == 200) {
      print(apiResponse.valor);
    } else {
      print("error de la consulta notas");
    }
/*
    if (apiResponse.codigo == 200) {
      _initialListProgram = (apiResponse.valor as List)
          .map((i) => ProgramasUModel.fromJson(i))
          .toList();
      _listProgram.add(_initialListProgram);
    } else {
      _initialListProgram = List<ProgramasUModel>();
      _listProgram.add(_initialListProgram);
      if (apiResponse.codigo == ConstantsLabels.codeErrorJWTEpired) {
        CommonsWidget().alertDialogExceptionExpired(
            context, ConstantsLabels.atencionLabel, apiResponse.mensaje);
      } else {
        CommonsWidget().showAlertDialogOneButton(
            context, ConstantsLabels.atencionLabel, apiResponse.mensaje, false);
      }
    }*/
    print("esperemos");
  }*/

//---------------------------------------------//

  Future<ObjApiResponse> programaAcademico(context) async {
    /* String pegeId = await _consultarNotasRepository
        .getValueLocalSotage(ConstantsLabels.keyPegeId);
    String tokenAccess = await _consultarNotasRepository
        .getValueLocalSotage(ConstantsLabels.keyToken);*/
    SharedPreferences elpegeId = await SharedPreferences.getInstance();
    SharedPreferences eltokenAccess = await SharedPreferences.getInstance();

    var pegeId = (elpegeId.getString('pegeId') ?? 0);
    var tokenAccess = (eltokenAccess.getString('tokenAccess') ?? 0);
    ObjApiResponse apiResponse =
        await _consultApiservice.programaAcademico(pegeId, tokenAccess);
    //  var programaAcademicoModel;
    if (apiResponse.codigo == 200) {
      _initialList1 = (apiResponse.valor as List)
          .map((i) => ProgramaAcademicoModel.fromJson(i))
          .toList();

      _list1.add(_initialList1);
      ;
    } else {
      _initialList1 = List<ProgramaAcademicoModel>();
      _list1.add(_initialList1);

      print("i");
    }
    if (apiResponse.codigo == 400) {
    } else if (apiResponse.codigo == 401) {
    } else if (apiResponse.codigo == 403) {
    } else if (apiResponse.codigo == 404) {
    } else if (apiResponse.codigo == 500) {}

    return apiResponse;
  }
}
