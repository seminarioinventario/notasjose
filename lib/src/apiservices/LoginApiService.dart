import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:notas_uniajc/src/models/ObjApiResponse.dart';
import 'package:notas_uniajc/src/models/session/RefreshToken.dart';
import 'package:notas_uniajc/src/models/session/SessionCredential.dart';
import 'package:notas_uniajc/src/models/session/SessionToken.dart';
import 'package:notas_uniajc/src/models/session/SessionUser.dart';
import 'package:notas_uniajc/src/utils/ConstantsLabels.dart';
import 'package:notas_uniajc/src/utils/ConstantsUrl.dart';
import 'package:http/http.dart' as http;

class LoginApiService {
  ObjApiResponse _objApiResponse;
  LoginApiService();

  Future<ObjApiResponse> authenticateCredential(
      SessionCredential sessionCredential) async {
    _objApiResponse = ObjApiResponse();
    var body2 = json.encode(sessionCredential.toJson());
    Uri uri = Uri.http(
        ConstantsUrl.serverUrl1, ConstantsUrl.authenticateCredentialUrl);

    try {
      var res = await http.post(uri,
          headers: {HttpHeaders.contentTypeHeader: "application/json"},
          body: body2);

      var resBody = json.decode(res.body);
      _objApiResponse = ObjApiResponse.fromJson(resBody);
    } on SocketException catch (_) {
      _objApiResponse.codigo = ConstantsLabels.codeErrorInternet;
      _objApiResponse.mensaje = ConstantsLabels.msgNotConnectioInternet;
    } on FormatException catch (_) {
      _objApiResponse.codigo = ConstantsLabels.codeErrorApi;
      _objApiResponse.mensaje = ConstantsLabels.msgNotConnectioApi;
    }

    return _objApiResponse;
  }

  Future<ObjApiResponse> validateUser(SessionUser sessionUser) async {
    _objApiResponse = ObjApiResponse();
    var body2 = json.encode(sessionUser.toJson());
    Uri uri = Uri.http(ConstantsUrl.serverUrl1, ConstantsUrl.validateUserUrl);

    try {
      var res = await http.post(uri,
          headers: {HttpHeaders.contentTypeHeader: "application/json"},
          body: body2);

      var resBody = json.decode(res.body);
      _objApiResponse = ObjApiResponse.fromJson(resBody);
    } on SocketException catch (_) {
      _objApiResponse.codigo = ConstantsLabels.codeErrorInternet;
      _objApiResponse.mensaje = ConstantsLabels.msgNotConnectioInternet;
    } on FormatException catch (_) {
      _objApiResponse.codigo = ConstantsLabels.codeErrorApi;
      _objApiResponse.mensaje = ConstantsLabels.msgNotConnectioApi;
    }

    return _objApiResponse;
  }

  Future<ObjApiResponse> olvideClave(SessionUser sessionUser) async {
    _objApiResponse = ObjApiResponse();
    var body2 = json.encode(sessionUser.toJson());
    Uri uri = Uri.http(ConstantsUrl.serverUrl1, ConstantsUrl.olvideClaveUrl);

    try {
      var res = await http.post(uri,
          headers: {HttpHeaders.contentTypeHeader: "application/json"},
          body: body2);

      var resBody = json.decode(res.body);
      _objApiResponse = ObjApiResponse.fromJson(resBody);
    } on SocketException catch (_) {
      _objApiResponse.codigo = ConstantsLabels.codeErrorInternet;
      _objApiResponse.mensaje = ConstantsLabels.msgNotConnectioInternet;
    } on FormatException catch (_) {
      _objApiResponse.codigo = ConstantsLabels.codeErrorApi;
      _objApiResponse.mensaje = ConstantsLabels.msgNotConnectioApi;
    }

    return _objApiResponse;
  }
}
