import 'dart:convert';

import 'dart:io';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:notas_uniajc/src/models/ObjApiResponse.dart';
import 'package:notas_uniajc/src/models/SessionNotas/HistoricoNotasModel.dart';
import 'package:notas_uniajc/src/models/SessionNotas/NotasActualesModel.dart';
import 'package:notas_uniajc/src/models/SessionNotas/ProgramaAcaModel.dart';
import 'package:notas_uniajc/src/utils/CommonsWidget.dart';
import 'package:notas_uniajc/src/utils/ConstantsLabels.dart';
import 'package:notas_uniajc/src/utils/ConstantsUrl.dart';
import 'package:notas_uniajc/src/Bloc/LoginBloc.dart';
import 'package:notas_uniajc/src/utils/LocalObj_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../repository/ConsultarNotaslRepository.dart';

class ConsultApiservice {
  ObjApiResponse _objApiResponse;
  ConsultarNotasRepository _consultarNotasRepository;
  NotasActualesModel notasActualesModel;
  ConsultApiservice();

  Future<ObjApiResponse> historicoNotas(
      String pegeId, String tokenAccess) async {
    String pegeId = await _consultarNotasRepository
        .getValueLocalSotage(ConstantsLabels.keyPegeId);

    String tokenAccess = await _consultarNotasRepository
        .getValueLocalSotage(ConstantsLabels.keyToken);

    Uri uri = Uri.http(ConstantsUrl.consultarNota,
        ConstantsUrl.historicoNotaUrl + pegeId + "/");
    _objApiResponse = ObjApiResponse();
    print(_objApiResponse.valor.toString());
    try {
      var res = await http.get(
        uri,
        headers: {
          HttpHeaders.authorizationHeader: tokenAccess,
          HttpHeaders.contentTypeHeader: "application/json"
        },
      );
      // _objApiResponse = _objApiResponse(res);
    } on SocketException catch (_) {
      _objApiResponse.codigo = ConstantsLabels.codeErrorInternet;
      _objApiResponse.mensaje = ConstantsLabels.msgNotConnectioInternet;
    } on FormatException catch (_) {
      _objApiResponse.codigo = ConstantsLabels.codeErrorApi;
      _objApiResponse.mensaje = ConstantsLabels.msgNotConnectioApi;
    }
    return _objApiResponse;
  }
  /*Future<ObjApiResponse> historicoNotas(String pegeId, String codigo) async {
    final pegeId = await _consultarNotasRepository
        .getValueLocalSotage(ConstantsLabels.keyPegeId);
    Uri uri = Uri.http(ConstantsUrl.consultarNota,
        ConstantsUrl.historicoNotaUrl + "/" + pegeId + "/" + codigo);
    _objApiResponse = ObjApiResponse();
    print(_objApiResponse.valor.toString());
    try {
      var res = await http.get(
        uri,
        headers: {
          HttpHeaders.authorizationHeader: "tokenAccess",
          HttpHeaders.contentTypeHeader: "application/json"
        },
      );
      // _objApiResponse = _objApiResponse(res);
    } on SocketException catch (_) {
      _objApiResponse.codigo = ConstantsLabels.codeErrorInternet;
      _objApiResponse.mensaje = ConstantsLabels.msgNotConnectioInternet;
    } on FormatException catch (_) {
      _objApiResponse.codigo = ConstantsLabels.codeErrorApi;
      _objApiResponse.mensaje = ConstantsLabels.msgNotConnectioApi;
    }
    return _objApiResponse;
  }*/

//codigo Ronald
  /*Future<ObjApiResponse> notasActual(String tokenAccess, String pegeId) async {
    //Uri uri = Uri.http(ConstantsUrl.consultarNota, ConstantsUrl.notasActualUrl + "/" + pegeId);
    Uri uri = Uri.http(
        "http://smartcampus.uniajc.edu.co:8888/rse_notasuniajc-1.0-SNAPSHOT/",
        "notasactual" + "/" + pegeId);
    _objApiResponse = ObjApiResponse();
    try {
      var res = await http.get(
        uri,
        headers: {
          HttpHeaders.authorizationHeader: tokenAccess,
          HttpHeaders.contentTypeHeader: "application/json"
        },
      );

      print("TokenConsultarNotaservice." + tokenAccess);
      print("pegeIdConsultarNotaservice." + pegeId);
      _objApiResponse = CommonsWidget().objResponse(res);
      print("consultarNotaservice." + _objApiResponse.valor);
    } on SocketException catch (_) {
      _objApiResponse.codigo = ConstantsLabels.codeErrorInternet;
      _objApiResponse.mensaje = ConstantsLabels.msgNotConnectioInternet;
    } on FormatException catch (_) {
      _objApiResponse.codigo = ConstantsLabels.codeErrorApi;
      _objApiResponse.mensaje = ConstantsLabels.msgNotConnectioApi;
    }
    return _objApiResponse;
  }*/

  Future<ObjApiResponse> notasActual(String pegeId, String tokenAcces) async {
    SharedPreferences _pegeId = await SharedPreferences.getInstance();
    SharedPreferences _tokenAccess = await SharedPreferences.getInstance();

    //Return String
    //_pegeId = (_pegeId.getString('pegeId') ?? 0);
    //print("eses es el pegeid");
    //print(_pegeId);
    //_tokenAccess = (_tokenAccess.getString('tokenAccess') ?? 0);
    //tokenAccess = _tokenAccess.toString();
    //pegeId = _pegeId.toString();
    //print("pegeidimpreso:");
    //print('Pressed $_pegeIdShared times.');
    //await _pegeId.setInt('_pegeIdShared', _pegeIdShared);
    //print(_pegeId.toString());
    //print("tockenimpreso:" + _tokenAccess.toString());
    //var pegeIdQuemado = "74409";
    var tokenAccessQuemado =
        "eyJhbGciOiJIUzUxMiJ9.eyJhY2MiOjUwNTQwLCJzdWIiOiJmYWNpZnVlbnRlcyIsImNyZWF0ZWQiOjE1ODg2OTI0Nzg5NDgsImlhdCI6MTU4ODY5MjQ3OH0.eAZGmYNLs4F6Vut2a9eec1MMiAD6wFfHNp6tG0m-YnBRn9N1FwiKb39-I3j80a3pypRd-W1pspFxIUVL6mRPIQ";
    _objApiResponse = ObjApiResponse();
    print(ConstantsUrl.consultarNota);
    // String uri =
    //  "http://smartcampus.uniajc.edu.co:8888/rse_notasuniajc-1.0-SNAPSHOT/notasactual/" +
    //    pegeId;
    Uri uri =
        Uri.http(ConstantsUrl.serverUrl2, ConstantsUrl.notasActualUrl + pegeId);
    //"smartcampus.uniajc.edu.co:8888",
    //"/rse_notasuniajc-1.0-SNAPSHOT/" + "notasactual/" + pegeId);
    //print("token en el Login: "+tokenAcces);
    print("despuesdeuri" + uri.toString());
    try {
      //Map<String, String> headers = content-typeHeader
      //var respuesta = await http.get(uri);
      var res = await http.get(
        uri,
        headers: {
          HttpHeaders.authorizationHeader: tokenAccessQuemado,
          HttpHeaders.contentTypeHeader: "application/json"
        },
      );

      // print(res.body);
      //print(_objApiResponse.valor);
      //_objApiResponse = CommonsWidget().objResponse(res.body);
      print("notas actuales userprueba:userprueba1 ");

      //_objApiResponse = json.decode(resBody.toString());
      //var resBody = json.decode(res.body);
      var resBody = json.decode(await getJson());
      _objApiResponse = ObjApiResponse.fromJson(resBody);
      print(resBody);
      print("notas actuales userprueba: userprueba2");

      print(_objApiResponse.toJson());
      //._objApiResponse(res);
    } on SocketException catch (_) {
      _objApiResponse.codigo = ConstantsLabels.codeErrorInternet;
      _objApiResponse.mensaje = ConstantsLabels.msgNotConnectioInternet;
    } on FormatException catch (_) {
      _objApiResponse.codigo = ConstantsLabels.codeErrorApi;
      _objApiResponse.mensaje = ConstantsLabels.msgNotConnectioApi;
    }
    return _objApiResponse;
  }

  Future<String> getJson() {
    return rootBundle.loadString("lib/src/resources/files/notas_actuales.json");
  }

  Future<ObjApiResponse> programaAcademico(
      String pegeId, String tokenAcces) async {
    //SharedPreferences _pegeId = await SharedPreferences.getInstance();

    //_pegeId = (_pegeId.getString('pegeId') ?? 0);
    var tokenAccesQuemado =
        "eyJhbGciOiJIUzUxMiJ9.eyJhY2MiOjUwNTQwLCJzdWIiOiJmYWNpZnVlbnRlcyIsImNyZWF0ZWQiOjE1ODg2OTI0Nzg5NDgsImlhdCI6MTU4ODY5MjQ3OH0.eAZGmYNLs4F6Vut2a9eec1MMiAD6wFfHNp6tG0m-YnBRn9N1FwiKb39-I3j80a3pypRd-W1pspFxIUVL6mRPIQ";
    Uri uri = Uri.http(
        ConstantsUrl.serverUrl2, ConstantsUrl.programaAcademicoUrl + pegeId);
    //_objApiResponse = ObjApiResponse();

    try {
      var res = await http.get(
        uri,
        headers: {
          HttpHeaders.authorizationHeader: tokenAccesQuemado,
          HttpHeaders.contentTypeHeader: "application/json"
        },
      );
      var resBody = json.decode(res.body);

      _objApiResponse = ObjApiResponse.fromJson(resBody);
      // _objApiResponse = _objApiResponse(res);
    } on SocketException catch (_) {
      _objApiResponse.codigo = ConstantsLabels.codeErrorInternet;
      _objApiResponse.mensaje = ConstantsLabels.msgNotConnectioInternet;
    } on FormatException catch (_) {
      _objApiResponse.codigo = ConstantsLabels.codeErrorApi;
      _objApiResponse.mensaje = ConstantsLabels.msgNotConnectioApi;
    }
    return _objApiResponse;
  }
}
